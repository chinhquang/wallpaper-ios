//
//  ViewController.swift
//  WallPaper
//
//  Created by Chính Trình Quang on 11/19/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit

class ViewController: UIViewController ,UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource{
    static var list : [String] = []
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    static var hdURLList : [String] = []
    static var hdURL : String = String()
    var refreshControl : UIRefreshControl = UIRefreshControl()
    func AddPullToRefresh (){
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        table.addSubview(refreshControl) // not required when using UITableViewController
    }
    @objc func refresh() {
        let apikey : String = "10735495-1120679f68cb349cf4e0d687e"
        searchTerm = searchBar.text! //"cat"
        let x = "https://pixabay.com/api/?key=" + apikey + "&q=" + searchTerm.encodeURIComponent()!
        let url = URL(string: x)
        
        
        
        let task = URLSession.shared.dataTask(with: url!) {(data, response, error) in
            guard let data = data else { return }
            print("json  nek : ");
            
            print(String(data: data, encoding: .utf8)!)
            
            do{
                let pData = try  JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String : Any]
                var i = 0;
                for(key,value) in  pData{
                    print("\(i)")
                    
                    
                    if(key == "hits"){
                        if let propsList : [[String : Any]] = value as? [[String : Any]]{
                            for dict in propsList{
                                
                                for (key,value)in dict{
                                    if(key == "previewURL"){
                                        print("\(value)");
                                        ViewController.list.append("\(value)")
                                        
                                    }else if(key == "largeImageURL"){
                                        ViewController.hdURLList.append("\(value)")
                                    }
                                }
                            }
                            
                            DispatchQueue.main.async(execute: { () -> Void in
                                
                                self.refreshControl.endRefreshing();
                                self.table.reloadData()
                            })
                        }
                    }
                    i = i + 1
                }
                
            }catch {
                print("Error")
            }
            
            
        }
        task.resume();
        refreshControl.endRefreshing();
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return ViewController.list.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        ViewController.hdURL = ViewController.hdURLList[indexPath.row]
        performSegue(withIdentifier: "detailSegue", sender: self)
        
    }
    func startXoay(){
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        activityIndicator.style = .gray
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        self.view.addSubview(activityIndicator)
        DispatchQueue.main.async {
            
            self.activityIndicator.startAnimating()
        }
        
    }
    func stopXoay(){
        activityIndicator.stopAnimating()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = ViewController.list[indexPath.row]
        let cell =  table.dequeueReusableCell(withIdentifier: "CustomTableViewCell") as! CustomTableViewCell
        
        
        cell.setCell(url: item);
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let url = URL(string: ViewController.list[indexPath.row])
        let data = try? Data(contentsOf: url!) //
        let image = UIImage(data: data!)
        let percentage = CGFloat((image?.size.width)!/(image?.size.height)!)
        
        return table.frame.width / percentage
    }
    
    
    @IBOutlet weak var table: UITableView!
    var searchTerm : String = String()
   
    @IBOutlet weak var searchBar: UISearchBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        UI.addDoneButton(controls: [searchBar])
        self.searchBar.delegate = self
        navigationItem.titleView = self.searchBar
        searchBar.showsScopeBar = false // you can show/hide this dependant on your layout
        searchBar.placeholder = "Search Image by Terms"
        self.table.delegate = self
        self.table.dataSource = self
        self.table.rowHeight = 280;
        setDataTableFromURL()
        AddPullToRefresh()
        
    }
    func setDataTableFromURL(){
        startXoay()
        let apikey : String = "10735495-1120679f68cb349cf4e0d687e"
        searchTerm = searchBar.text! //"cat"
        let x = "https://pixabay.com/api/?key=" + apikey + "&q=" + searchTerm.encodeURIComponent()!
        let url = URL(string: x)
        
        
        
        let task = URLSession.shared.dataTask(with: url!) {(data, response, error) in
            guard let data = data else { return }
            print("json  nek : ");
            
            print(String(data: data, encoding: .utf8)!)
            
            do{
                let pData = try  JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String : Any]
                var i = 0;
                for(key,value) in  pData{
                    print("\(i)")
                    
                    
                    if(key == "hits"){
                        if let propsList : [[String : Any]] = value as? [[String : Any]]{
                            for dict in propsList{
                                
                                for (key,value)in dict{
                                    if(key == "previewURL"){
                                        print("\(value)");
                                        ViewController.list.append("\(value)")
                                        
                                    }else if(key == "largeImageURL"){
                                        ViewController.hdURLList.append("\(value)")
                                    }
                                }
                            }
                            
                            DispatchQueue.main.async(execute: { () -> Void in
                                
                                self.stopXoay()
                                self.table.reloadData()
                            })
                        }
                    }
                    i = i + 1
                }
                
            }catch {
                print("Error")
            }
            
            
        }
        task.resume();
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        startXoay()
        ViewController.list = []
        ViewController.hdURLList = []
        self.searchBar.endEditing(true)
        
        setDataTableFromURL()
        
    }
    
}



extension String {
    func encodeURIComponent() -> String? {
        var characterSet = CharacterSet.alphanumerics
        characterSet.insert(charactersIn: "-_.!~*'()")
        return self.addingPercentEncoding(withAllowedCharacters: characterSet)
    }
}

