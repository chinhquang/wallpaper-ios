//
//  Keyboard.swift
//  WallPaper
//
//  Created by Chính Trình Quang on 11/20/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//
import UIKit
class UI{
    static func addDoneButton (controls : [UITextField]){
        for textField in controls {
            let toolbar = UIToolbar();
            toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil),UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target:textField, action: #selector(UITextField.resignFirstResponder))];
            toolbar.sizeToFit();
            textField.inputAccessoryView = toolbar;
        }
        
    }
    static func addDoneButton (controls : [UISearchBar]){
        for textField in controls {
            let toolbar = UIToolbar();
            toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil),UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target:textField, action: #selector(UITextField.resignFirstResponder))];
            toolbar.sizeToFit();
            textField.inputAccessoryView = toolbar;
        }
        
    }
}

