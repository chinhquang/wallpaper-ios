//
//  CustomTableViewCell.swift
//  WallPaper
//
//  Created by Chính Trình Quang on 11/19/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit
class CustomTableViewCell : UITableViewCell{
    
    
    @IBOutlet weak var imageLoad: UIImageView!
    func setCell (url : String){
        let Url = URL(string: url)
        let data = try? Data(contentsOf: Url!) //
        imageLoad.image = UIImage(data: data!)
        
        
        
    }
}
