//
//  DetailViewController.swift
//  WallPaper
//
//  Created by Chính Trình Quang on 11/20/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit
class DetailViewController: UIViewController,UIImagePickerControllerDelegate{
     var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    var url : String = String()
    @IBOutlet weak var imageLoad: UIImageView!
    @IBOutlet weak var downloadBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var zoomOutBtn: UIButton!
    
    
    @IBAction func downloadImage(_ sender: Any) {
        UIImageWriteToSavedPhotosAlbum(imageLoad.image!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    @IBAction func shareImage(_ sender: Any) {
        let itemshare : UIImage = imageLoad.image!
        let activityVC = UIActivityViewController(activityItems: [itemshare], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
//        activityVC.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop,UIActivity.ActivityType.postToFacebook ]
        self.present(activityVC, animated: true, completion: nil)
    }
    //MARK: - Add image to Library
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLayOut()
        startXoay()
        url = ViewController.hdURL
        
        setCell(url: url)
        stopXoay()
        
    }
    func setLayOut(){
        downloadBtn.layer.borderWidth = 1
        downloadBtn.layer.cornerRadius = 3
        downloadBtn.contentEdgeInsets.top = 5
        downloadBtn.contentEdgeInsets.left = 5
        downloadBtn.contentEdgeInsets.right = 5
        downloadBtn.contentEdgeInsets.bottom = 5
        
        shareBtn.layer.borderWidth = 1
        shareBtn.layer.cornerRadius = 3
        shareBtn.contentEdgeInsets.top = 5
        shareBtn.contentEdgeInsets.left = 5
        shareBtn.contentEdgeInsets.right = 5
        shareBtn.contentEdgeInsets.bottom = 5
        
        zoomOutBtn.layer.borderWidth = 1
        zoomOutBtn.layer.cornerRadius = 3
        zoomOutBtn.contentEdgeInsets.top = 5
        zoomOutBtn.contentEdgeInsets.left = 5
        zoomOutBtn.contentEdgeInsets.right = 5
        zoomOutBtn.contentEdgeInsets.bottom = 5
        
    }
    func startXoay(){
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        activityIndicator.style = .gray
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        self.view.addSubview(activityIndicator)
        DispatchQueue.main.async {
            
            self.activityIndicator.startAnimating()
        }
        
    }
    func stopXoay(){
        DispatchQueue.main.async {
            
            self.activityIndicator.stopAnimating()
        }
        
    }
    func setCell (url : String){
        
        let Url = URL(string: url)
        let data = try? Data(contentsOf: Url!) //
       
        imageLoad.image = UIImage(data: data!)
        
    }
    
    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
   
}
